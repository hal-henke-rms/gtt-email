import pyodbc 
from sqlalchemy import create_engine
from sqlalchemy.pool import StaticPool
import logging
logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)
import configparser

import time
import datetime
from datetime import timezone
import smtplib
from email.message import EmailMessage


CONFIG_FILE = "./config.ini"

config = configparser.ConfigParser()
config.read(CONFIG_FILE)

USER = config['LOCAL']['USER']
PASSWORD = config['LOCAL']['PASSWORD']
SERVER = config['LOCAL']['SERVER']
DATABASE = config['LOCAL']['DATABASE']
TIMEOUT_MINUTES = int(config['LOCAL']['TIMEOUT_MINUTES'])

print("Connecting to Database...")

def connect_and_query():
    """
    Connect to the database, query the `VirtualSegmentsTravelTime` table 
    for the latest update & return the result
    """
    CONN_STRING = 'DRIVER={ODBC Driver 13 for SQL Server};SERVER='+SERVER+';\
    PORT=1443;DATABASE='+DATABASE+';\
    UID=' + USER + ';PWD='+ PASSWORD + ';\
    Trusted_connection=no'
    print("connection string is: ", CONN_STRING)
    cnxn = pyodbc.connect(CONN_STRING)
    print('Connected to DataBase: ' + DATABASE)
    cursor = cnxn.cursor()
    eng = create_engine("mssql+pyodbc://?driver = ODBC+Driver+13+for+SQL+Server", poolclass=StaticPool, creator=cnxn)

    result = cnxn.execute("SELECT MAX([BatchDTG]) FROM [dbo].[VirtualSegmentsTravelTime]")
    gtime = list(enumerate(result))[0][1][0]
    result.close()
    return gtime

def compare_times(gtt):
    """
    Check if a given Google Travel Time occurred before or after 
    the threshold established by TIMEOUT_MINUTES
    """
    d_fail = datetime.timedelta(minutes=TIMEOUT_MINUTES)    
    now = datetime.datetime.now()
    gtt_delta = now - gtt
    return d_fail > gtt_delta


def makeEmail(most_recent_GTT):
    """
    Constructs the email to be sent when GTT is not polling properly.
    """
    FROM_ADDRESS = config['SMTP']['FROM_ADDRESS']
    TARGET_ADDRESS = config['SMTP']['TARGET_ADDRESS']

    msg = EmailMessage()
    gtt_local = most_recent_GTT.replace(tzinfo=timezone.utc).astimezone(tz=None)
    msg.set_content("WARNING: No Google Travel Time information has been retrieved in the past {} minutes.\n\nThe last record sucesfully stored in the database was retrieved at {}, local time.\n\nTo pause/disable this alert go to the Windows Task Scheduler on Unicorn and alter the task called 'GTT FAILURE.'\n".format(TIMEOUT_MINUTES, gtt_local.strftime("%d-%m-%Y - %H:%M:%S")))
    msg['Subject'] = "Google Travel Time Failure"
    msg['From'] = FROM_ADDRESS 
    msg['To'] = TARGET_ADDRESS
    return msg


def sendEmail(msg):
    """
    Takes an Email message and sends it to address as set in config file     
    """
    SMTP_SERVER = config['SMTP']['SMTP_SERVER']
    SMTP_PORT = config['SMTP']['SMTP_PORT']

    print("Sending email notification of GTT Failure...")
    s = smtplib.SMTP(SMTP_SERVER, port=SMTP_PORT)
    s.send_message(msg)
    s.quit()

def main():
    gtime = connect_and_query()
    all_good = compare_times(gtime)
    if all_good:
        print("Its all good timing wise...")
    else:
        print("Its all bad timing wise...")
        msg = makeEmail(gtime)
        sendEmail(msg)

if __name__ == '__main__':
    main()